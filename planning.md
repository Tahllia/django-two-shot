**Django Two-Shot - Feature 1
*[x]Fork and clone the starter project from Django Two-Shot 
*[x]Create a new virtual environment in the repository directory for the project
*[x]Activate the virtual environment
*[x]Upgrade pip
*[x]Install django
*[x]Install black
*[x]Install flake8
*[x]Install djhtml
*[x]Install djlint
*[x]Deactivate your virtual environment
*[x]Activate your virtual environment
*[x]Use pip freeze to generate a requirements.txt file
*[x]Create a Django project named "expenses" by running the command (see below) in the directory of your cloned repo after which your cloned repo directory should have these top-level directories and file:
.
├── .git
├── .gitignore
├── .venv
├── expenses
├── manage.py
├── pyproject.toml
├── requirements.txt
└── tests

**Feature 2 - Apps
*[x]Create a Django app named accounts and install it in the expenses Django project in the INSTALLED_APPS list
*[x]Create a Django app named receipts and install it in the expenses Django project in the INSTALLED_APPS list
*[x]Run the migrations
*[x]Create a super user

**Feature 3 - Models
*[x]Create these three models with the specified features:

*[x]ExpenseCategory
*[x]Receipts
*[x]Account


**Feature 4 - Admin
*[x]Register the three models with the admin so that you can see them in the Django admin site.

**Feature 5 - Receipt ListView
*[x]Create a view that will get all of the instances of the Receipt model and put them in the context for the template.

*[x]Register that view in the receipts app for the path "" and the name "home" in a new file named receipts/urls.py.
    ***REMINDER**
        *[X]UPDATE INSIDE OF THE PROJECT URLS AS WELL
*[x]Include the URL patterns from the receipts app in the expenses project with the prefix "receipts/".
*[x]Create a template for the list view that complies with the following specifications.
should result in HTML that has:

the fundamental five in it
a main tag that contains:
an h1 tag with the content "My Receipts"
a table that has 6 columns:
the first with the header "Vendor" and the rows with the vendor name of the receipt
the second with the header "Total" and the total value from the receipt because we don't yet have tasks
The third with the header "Tax" and the tax value from the receipt
The fourth with the header "Date" and the date value formatted as "m/d/YY"
the fifth with the header "Category" and the name of the category for the receipt
The sixth with the header "Account" and the name of the account for the receipt
     ***REMINDER**
        *[X]Don't forget to put the h1 tag immediatley under main 

Feature 6 - Home
*[x]In the expenses urls.py, use the RedirectView to redirect from "" to the name of the path for the list view that you created in the previous feature. Register that path a name of "home".
    *[x]from django.urls import include reverse_lazy
    *[x]from django.views.generic.base import RedirectView
    *[x] path("", RedirectView.as_view(url=reverse_lazy("home"))),

**Feature 7 - LoginView

*[x]Register the LoginView  in your accounts urls.py with the path "login/" and the name "login".
*[x]Include the URL patterns from the accounts app in the expenses project with the prefix "accounts/".
*[x]Create a templates directory under accounts.
*[x]Create a registration directory under templates.
*[x]Create an HTML template named login.html in the registration directory.
*x[]Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below).
*[x]In the expenses settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home", which will redirect us to the path (not yet created) with the name "home".

**Feature 8 - Filtering Receipts
*[x]Protect the list view for the Receipt model so that only a person who has logged in can access it.
    *[x]class ReceiptListView(LoginRequiredMixin, ListView)
        *[xx]See notes
*[x]Change the queryset of the view to filter the Receipt objects where purchaser equals the logged in user.
    *[]def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
*[]
*[]
*[]







**Feature 9 - Logout
*[x]In the accounts/urls.py file
    *[x]Import the LogoutView from the same module that you imported the LoginView from.
        *[x]from django.contrib.auth.views import LoginView, LogoutView

    *[x]Register that view in your urlpatterns list with the path "logout/" and the name "logout"
        *[x]path("logout/", LogoutView.as_view(), name="logout"),
*[x]In the expenses settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login", which will redirect the logout view to the login page.
    *[x]LOGOUT_REDIRECT_URL = "login"


**Feature 10 - Signup    
**This feature allows people to sign up for the project tracker.
*[x]You need to create a function view to handle showing the sign-up form and handle its submission. This is a view, so you should create it in the file in the accounts directory that should hold the views.

*[]You'll need to import the UserCreationForm from the built-in auth forms
*[]You'll need to use the special create_user  method to create a new user account from their username and password.
*[]You'll need to use the login  function that logs an account in.
*[]After you've created the user, redirect the browser to the path registered with the name "home".
*[]Create an HTML template named signup.html in the registration directory.
*[]Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below).


**Feature 11 - Create Receipt View
*[x]Create a create view for the Receipt model that will show the vendor, total, tax, date, category, and account properties in the form and handle the form submission to create a new Receipt.
*[x]A person must be logged in to see the view.
    *[x]If you're using class views, you can set the user by writing your own form_valid method. The general form of a method that assigns the current user to a User property looks like this:
        *[x]def form_valid(self, form):
        item = form.save(commit=False)
        item.user_property = self.request.user
        item.save()
        return redirect("some_view")
*[x]Register that view for the path "create/" in the receipts urls.py and the name "create_receipt".
*[x]Create an HTML template that shows the form to create a new Receipt (see the template specifications below).


*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]
*[]


*[]
*[]
*[]
*[]
*[]
